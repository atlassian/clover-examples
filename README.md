Information
======================

This repository contains code examples showing how to integrate Clover with various tools, 
such as Ant, Maven, Groovy, Spock etc. 

Examples are open-source, based on the Apache License version 2.0. 

Atlassian Clover links
======================

Documentation: http://confluence.atlassian.com/display/CLOVER/Clover+Documentation+Home

Issue tracker: http://jira.atlassian.com/browse/CLOV

Technical support: http://support.atlassian.com

Download page: http://www.atlassian.com/software/clover/download

Clover home page: http://www.atlassian.com/clover 